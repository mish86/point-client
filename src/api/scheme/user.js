import * as yup from 'yup';

/*
{
    "id": <i32>,
    "username": <string>,
    "realname": ?<string>,
    "password": ?<string>,
}
*/

const basic = {
    username: yup.string().label('Email').default('').required(),
    password: yup.string().label('Password').default('').required(),
};

const create = yup.object().shape({
    ...basic,
    realname: yup.string().label('First Name').default('').required(),
});

const existing = yup.object().shape({
    ...basic,
    id: yup.number().positive().integer().required(),
});

export default {
    normalization: { },
    default: {
        create: create,
        basic: yup.object().shape({
            ...basic,
        }),
    },
    validation: {
        create,
        existing,
        basic: yup.object().shape({
            ...basic,
        }),
    },
};