import { schema } from 'normalizr';
import * as yup from 'yup';

/*
{
    "id": <i32>,
    "summary": <string>,
    "amount": <f32>,
    "description": ?<string>,
}
normilize:
{
    result: id,
    entities: {
        "services": {
            id: {
                "id": <i32>,
                "summary": <string>,
                "amount": <f32>,
                "description": ?<string>,
            }
        }
    }
}
*/
const normal = new schema.Entity(
    'services',
    {},
    {
        processStrategy: entity => {
            return {
                ...entity,
                meta: {
                    isSync: true,
                    isLoading: false,
                    isError: false,
                    error: null,
                }
            }
        }
    },
);

const unsync = new schema.Entity(
    'services',
    {},
    {
        processStrategy: entity => {
            return {
                ...entity,
                meta: {
                    isSync: false,
                    isLoading: false,
                    isError: false,
                    error: null,
                }
            }
        }
    },
);

const meta = yup.object().shape({
    isSync: yup.boolean().default(false),
    isLoading: yup.boolean().default(false),
    isError: yup.boolean().default(false),
    error: yup.mixed().default(null).nullable(),
});

const basic = {
    summary: yup.string().label('Summary').default('').required(),
    description: yup.string().label('Description').default('').nullable(),
    amount: yup.number().positive().label('Amount').default(0).required(),
};

const create = yup.object().shape({
    ...basic,
    id: yup.number().positive().integer().notRequired(),
});

const existing = yup.object().shape({
    ...basic,
    id: yup.number().positive().integer().required(),
});

export default {
    normalization: {
        normal,
        unsync,
    },
    default: yup.object().shape({
        ...basic,
        meta: meta,
    }),
    validation: {
        create,
        existing,
    },
};