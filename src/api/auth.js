import { 
    useState,
    createContext,
    useContext
} from 'react';

import HttpStatus from 'http-status-codes';
import RequestMethods from './request_methods';

export const LoginData = props => {
    const { username, password } = props;

    return {
        basic: {'Authorization': `Basic ${btoa(`${username}:${password}`)}`},
    }
}

export const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export const ErrorKind = Object.freeze({
    INVALID_USERNAME: Symbol('INVALID_USERNAME'),
    INVALID_PASSWORD: Symbol('INVALID_PASSWORD'),
    INVALID_CREDENTIALS: Symbol('INVALID_CREDENTIALS'),
});

const UseBasicAuth = props => {
    const { url, method, loading } = props;

    const [isLoading, setLoading] = useState(loading);
    const [error, setError] = useState({});
    const { setAuthorization } = useAuth();

    
    const call = (props, onSuccess, onFailure) => {

        const { headers, body } = props;

        if (!isLoading) {
            setLoading(true);
            setError({});
        }
        fetch(url, {
            method,
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cookie': document.cookie,
                ...headers,
            },
            body,
        })
        .then(async response => {
            if (response.ok) {
                return Promise.resolve(response.json());
            }
            const result = await Promise.resolve(response.json());
            let error = ((status) => {
                switch (status) {
                    case HttpStatus.NOT_FOUND:
                        return {
                            ...result,
                            kind: ErrorKind.INVALID_USERNAME,
                            'username': {
                                message: result.detail,
                                isError: true,
                            },
                        };
                    case HttpStatus.BAD_REQUEST:
                        return {
                            ...result,
                        };
                    case HttpStatus.UNAUTHORIZED:
                    default:
                        return {
                            ...result,
                            kind: ErrorKind.INVALID_CREDENTIALS,
                        };
                };
            })(response.status);
            return Promise.reject(error);
        })
        .then(
            (user) => {
                setError({});
                setAuthorization({
                    authorized: true,
                    user,
                });
                setLoading(false);
                if (onSuccess) onSuccess(user);
            },
            (error) => {
                setError(error);
                setAuthorization({
                    authorized: false,
                    user: undefined,
                });
                setLoading(false);
                if (onFailure) onFailure(error);
            }
        )
    }
    
    return { 
        isLoading, 
        error, 
        setError,
        call
    };
}

export const SignIn = () => {
    let api = UseBasicAuth({
        url: '/api/auth/login',
        method: RequestMethods.POST,
        loading: false,
    });

    return {
        ...api,
        signin: (loginData, onSuccess = undefined, onFailure = undefined) => 
            api.call({
                headers: { ...loginData.basic }
            }, onSuccess, onFailure),
    }
}

export const SignUp = () => {
    let api = UseBasicAuth({
        url: '/api/auth/signup',
        method: RequestMethods.POST,
        loading: false,
    });

    return {
        ...api,
        signup: (signupData, onSuccess = undefined, onFailure = undefined) => 
            api.call({
                body: JSON.stringify({ ...signupData }),
            }, onSuccess, onFailure),
    }
}

export const Ping = () => {
    let api = UseBasicAuth({
        url: '/api/auth',
        method: RequestMethods.POST,
        loading: true,
    });

    return {
        ...api,
        ping: (onSuccess = undefined, onFailure = undefined) => 
            api.call({}, onSuccess, onFailure),
    }
}