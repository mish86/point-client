import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { unifiedReducer as api } from './';

const loggerMiddleware = createLogger();

export const configureStore = () => {
    return createStore(
        api,
        applyMiddleware(thunkMiddleware, loggerMiddleware)
    )
};