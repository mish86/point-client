import { PullApiTypes } from '../actions';
import { normalize } from 'normalizr';
import _ from 'lodash';

export const pullStart = () => ({
    type: PullApiTypes.PULL_START
});

export const pullSuccess = (json, scheme) => ({
    type: PullApiTypes.PULL_SUCCESS,
    playload: normalize(json, [scheme]),
    scheme: scheme,
})

export const pullError = error => ({
    type: PullApiTypes.PULL_ERROR,
    error: error,
})

export const pullReducer = (state, action) => {
    switch (action.type) {
        case PullApiTypes.PULL_START: {
            return {
                ...state,
                isLoading: true,
                isError: false,
                error: null,
            }
        }
        case PullApiTypes.PULL_SUCCESS: {
            let keys = action.playload.result;
            if (keys.length <= 0) {
                return {
                    ...state,
                    isLoading: false,
                    isError: false,
                    error: null,
                }
            }

            let entities = _.merge(state.entities, action.playload.entities);
            entities[action.scheme.key] = _.pick(entities[action.scheme.key], keys);
            return {
                isLoading: false,
                isError: false,
                error: null,
                entities: entities,
            }
        }
        case PullApiTypes.PULL_ERROR: {
            return {
                ...state,
                isLoading: false,
                isError: true,
                error: action.error,
            }
        }
        default:
            return state;
    }
}