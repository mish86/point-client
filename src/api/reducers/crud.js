import { CrudApiTypes } from './../actions';
import { normalize } from 'normalizr';

import _ from 'lodash';

export const crudStart = (keys, subtype, scheme) => ({
    type: CrudApiTypes.CRUD_START,
    subtype,
    keys,
    scheme,
})

export const crudError = (keys, error, scheme) => ({
    type: CrudApiTypes.CRUD_ERROR,
    keys,
    error,
    scheme,
})

export const crudCreate = (_keys, json, scheme) => ({
    type: CrudApiTypes.CREATE,
    playload: normalize(json, scheme),
    scheme,
})

export const crudRead = (keys, json, scheme) => ({
    type: CrudApiTypes.READ,
    keys,
    playload: normalize(json, scheme),
    scheme,
})

export const crudDelete = (keys, _json, scheme) => ({
    type: CrudApiTypes.DELETE,
    keys,
    scheme,
})

export const crudPatch = (keys, json, scheme) => ({
    type: CrudApiTypes.PATCH,
    keys,
    playload: json && normalize(json, scheme),
    scheme,
})

export const crudReducer = (state, action) => {
    switch (action.type) {
        case CrudApiTypes.CRUD_START: {
            let keys = action.keys;
            if (keys.length <= 0) return state;

            let entities = _.pick(state.entities[action.scheme.key], keys);
            entities = _.mapValues(entities, entity => {
                return {
                    ...entity,
                    meta: {
                        isSync: false,
                        isLoading: true,
                        isError: false,
                        error: null,
                    },
                }
            });

            return {
                ...state,
                entities: {
                    [action.scheme.key]: {
                        ...state.entities[action.scheme.key],
                        ...entities,
                    }
                }
            }
        }
        case CrudApiTypes.CRUD_ERROR: {
            let keys = action.keys;
            if (keys.length <= 0) return state;

            let entities = _.pick(state.entities[action.scheme.key], keys);
            entities = _.mapValues(entities, entity => {
                return {
                    ...entity,
                    meta: {
                        isSync: false,
                        isLoading: false,
                        isError: true,
                        error: action.error,
                    },
                }
            });

            return {
                ...state,
                entities: {
                    [action.scheme.key]: {
                        ...state.entities[action.scheme.key],
                        ...entities,
                    }
                }
            }
        }
        case CrudApiTypes.CREATE: {
            let newState = {
                ...state,
                entities: {
                    [action.scheme.key]: {
                        ...state.entities[action.scheme.key],
                        ...action.playload.entities[action.scheme.key],
                    }
                }
            };

            delete newState.entities[action.scheme.key][undefined];
            return newState;
        }
        case CrudApiTypes.READ: {
            let keys = action.keys;
            if (keys.length <= 0) return state;

            return {
                ...state,
                entities: {
                    [action.scheme.key]: {
                        ...state.entities[action.scheme.key],
                        ...action.playload.entities[action.scheme.key],
                    }
                }
            }
        }
        case CrudApiTypes.DELETE: {
            let keys = action.keys;
            if (keys.length <= 0) return state;

            let entities = _.omit(state.entities[action.scheme.key], keys);

            return {
                ...state,
                entities: {
                    [action.scheme.key]: entities,
                }
            }
        }
        case CrudApiTypes.PATCH: {
            let keys = action.keys;
            if (keys.length <= 0) return state;

            return {
                ...state,
                entities: {
                    [action.scheme.key]: {
                        ...state.entities[action.scheme.key],
                        ...action.playload?.entities[action.scheme.key],
                    }
                }
            }
        }

        default:
            return state;
    }
};