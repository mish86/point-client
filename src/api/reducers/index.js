import {
    useDispatch,
} from 'react-redux';
import * as pull from './pull';
import * as crud from './crud';
import { CrudApiTypes } from './../actions';
import RequestMethods from '../request_methods';
import HttpStatus from 'http-status-codes';

export const unifiedReducer = (
    state = {
        entities: {},
        isLoading: false,
        isError: false,
        error: null,
    },
    action,
) => {
    // reducers should intersects by action type
    state = pull.pullReducer(state, action);
    state = crud.crudReducer(state, action);

    return state;
}

const basicCrudApi = (subtype, key, entity, props) => {

    let { scheme, url, method, contentType, onSuccess, onSuccessCallback, setAuthorization } = props;

    let headers = {
        'Accept': 'application/json',
        'Cookie': document.cookie,
    };
    if (contentType !== 'multipart/form-data') {
        headers['Content-Type'] = contentType;
    }

    return async dispatch => {

        let keys = [key];

        dispatch(crud.crudStart(keys, subtype, scheme));

        try {
            let response = await fetch(url, {
                method: method,
                credentials: 'same-origin',
                headers,
                body: entity,
            });

            let json = await response.json().catch(() => null);
            
            if (response.ok) {
                dispatch(onSuccess(keys, json, scheme));
                if (onSuccessCallback) onSuccessCallback();
            } else {
                switch (response.status) {
                    case HttpStatus.UNAUTHORIZED:
                        setAuthorization({
                            authorized: false,
                            user: undefined,
                        });
                    break;
                    default: break;
                }
                dispatch(crud.crudError(keys, json, scheme));
            }
            
        } catch (error) {
            dispatch(crud.crudError(keys, error, scheme));
        }
    }
}

const basicPullApi = (props) => {

    let { url, scheme, setAuthorization } = props;

    return async dispatch => {

        dispatch(pull.pullStart());

        try {
            let response = await fetch(url, {
                method: RequestMethods.GET,
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Cookie': document.cookie,
                },
            });
            
            let json = await response.json();

            if (response.ok) {
                dispatch(pull.pullSuccess(json, scheme));
            } else {
                switch (response.status) {
                    case HttpStatus.UNAUTHORIZED:
                        setAuthorization({
                            authorized: false,
                            user: undefined,
                        });
                    break;
                    default: break;
                }
                dispatch(pull.pullError(json));
            }
            
        } catch (error) {
            dispatch(pull.pullError(error));
        }
    }
}

export const CrudApi = (props) => {
    const { baseUrl, scheme, schemeNonSync, setAuthorization } = props;

    const dispatch = useDispatch();
    
    return {
        pull: () => {
            dispatch(basicPullApi({
                url: `${baseUrl}/list`,
                scheme,
                setAuthorization,
            }));
        },
        
        create: (tempKey, data, contentType = 'application/json', onSuccessCallback = undefined ) => {
            dispatch(basicCrudApi(CrudApiTypes.CREATE, tempKey, data, {
                scheme,
                url: `${baseUrl}`,
                method: RequestMethods.POST,
                contentType: contentType,
                onSuccess: crud.crudCreate,
                onSuccessCallback,
                setAuthorization,
            }));
        },

        read: (key, contentType = 'application/json', onSuccessCallback = undefined) => {

            dispatch(basicCrudApi(CrudApiTypes.READ, key, undefined, {
                scheme,
                url: `${baseUrl}/${key}`,
                method: RequestMethods.GET,
                contentType: contentType,
                onSuccess: crud.crudRead,
                onSuccessCallback,
                setAuthorization,
            }));
        },

        delete: (key) => {

            dispatch(basicCrudApi(CrudApiTypes.DELETE, key, undefined, {
                scheme,
                url: `${baseUrl}/${key}`,
                method: RequestMethods.DELETE,
                contentType: 'application/json',
                onSuccess: crud.crudDelete,
                setAuthorization,
            }));
        },

        edit: (key, data) => {
            dispatch(crud.crudPatch(
                [key],
                data,
                schemeNonSync,
            ));
        },

        patch: (key, data,
            onSuccessCallback = undefined,
            props = {},
        ) => {

            props = {
                scheme: scheme,
                contentType: 'application/json',
                url: `${baseUrl}/${key}`,
                method: RequestMethods.PATCH,
                ...props,
            };

            dispatch(basicCrudApi(CrudApiTypes.PATCH, key, data, {
                ...props,
                onSuccess: crud.crudPatch,
                onSuccessCallback,
                setAuthorization,
            }));
        },
    }
}