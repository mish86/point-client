import React from 'react';
import {
    useHistory,
	useLocation,
} from "react-router-dom";

import _ from 'lodash';

export default function BaseSignContainer(props) {
    const history = useHistory();
    const location = useLocation();
    let { from } = location.state || { from: { pathname: "/" } };

    const { component, validation, data, status, onSubmit } = props;
    const [dto, setDto] = data;
    const { isLoading, isOverallError, error, setError } = status;
    const Component = component;
    
    const handleFocus = (event) => {
        setError(_.pick(error, event.target.name));
    }

    const handleChange = (event) => {
        let nextState = {
			...dto,
			[event.target.name]: event.target.value,
        };
		setDto(nextState);
        
        validation.validateAt(event.target.name, nextState)
        .then(_data => setError({}))
		.catch(error => {
            setError({
                [error.path]: {
					message: error.message,
					isError: true,
				}
            });
        });
    };
    
    const handleSubmit = (event) => {
		event.preventDefault();

		validation.validate(dto, {
			strict: true,
			abortEarly: false,
		}).then(data => onSubmit(data, onSuccess, onFailure))
		.catch(error => {
			let extracted = _.chain(error.inner)
				.keyBy(error => error.path)
				.mapValues(error => { return {
					message: error.message,
					isError: true,
				}})
				.value();
            setError(extracted);
        });
    };
    
    const onSuccess = () => {
		history.replace(from);
    }
    
    const onFailure = (error) => {
		console.log(error);
	}
    
    return (
        <Component
            dto={dto}
            isLoading={isLoading}
            isOverallError={isOverallError}
            error={error}
            handleFocus={handleFocus}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
        />
    );
}