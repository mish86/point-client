import React, { 
	useState,
} from 'react';

import BaseSignForm from './BaseSignForm';
import SignInForm from './../components/SignInForm';

import { SignIn, LoginData, ErrorKind } from '../api/auth';
import scheme from '../api/scheme/user';

export default function SignInFormContainer() {
	const [dto, setDto] = useState(scheme.default.basic.default());
	const { isLoading, error, setError, signin } = SignIn();

	const overallErrorKinds = [ErrorKind.INVALID_CREDENTIALS];

	const onSubmit = async (data, onSuccessCallback, onFailureCallback) => {
        signin(LoginData(data), onSuccessCallback, onFailureCallback);
	}

	return (
        <BaseSignForm
            component={SignInForm}
            validation={scheme.validation.basic}
            data={[dto, setDto]}
            status={{
                isLoading,
                isOverallError: overallErrorKinds.includes(error.kind),
                error,
                setError,
            }}
            onSubmit={onSubmit}
        />
    );
}
