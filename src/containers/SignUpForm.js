import React, {
    useState,
} from 'react';

import BaseSignForm from './BaseSignForm';
import SignUpForm from './../components/SignUpForm';

import { SignUp as SignUpApi, ErrorKind } from './../api/auth'; 
import scheme from './../api/scheme/user';

export default function SignUpContainer() {
    const [dto, setDto] = useState(scheme.default.create.default());
    const { isLoading, error, setError, signup } = SignUpApi();

    const overallErrorKinds = [ErrorKind.INVALID_CREDENTIALS];

	const onSubmit = async (data, onSuccessCallback, onFailureCallback) => {
        signup(data, onSuccessCallback, onFailureCallback);
	}
    
    return (
        <BaseSignForm
            component={SignUpForm}
            validation={scheme.validation.create}
            data={[dto, setDto]}
            status={{
                isLoading,
                isOverallError: overallErrorKinds.includes(error.kind),
                error,
                setError,
            }}
            onSubmit={onSubmit}
        />
    );
}