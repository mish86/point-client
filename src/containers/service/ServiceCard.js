import React, {
} from 'react';
import {
	Link as RouterLink,
	useLocation,
} from "react-router-dom";
import {
    useSelector,
} from 'react-redux'

import CardView from '../../components/service/CardView';

import { useAuth } from "../../api/auth";
import { CrudApi } from '../../api';
import scheme from '../../api/scheme/service';

export default function ServiceCardContainer(props) {
	const location = useLocation();
	
	const id = props.entityId;
	const service = useSelector(state => state.entities[scheme.normalization.normal.key][id]);

    const { setAuthorization } = useAuth();
    
	const api = CrudApi({
        baseUrl: '/api/service',
        scheme: scheme.normalization.normal,
        schemeNonSync: scheme.normalization.unsync,
        setAuthorization,
    });
	
	const handleDelete = (event) => {
		event.preventDefault();

		api.delete(id);
	};
    
    return (
        <CardView
            id={id}
            service={service}
            photoHref={`/api/service/${service.id}/cover`}
            // handle   Edit={}
            handleDelete={handleDelete}
            editTo={{
                pathname: `/api/service/${service.id}`,
                state: { 
                    service: service,
                    from: location,
                },
            }}
            deleteTo=""
            RouterLink={RouterLink}
        />
    );
}