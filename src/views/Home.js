import React from 'react';

import Container from '@material-ui/core/Container';
import ServiceList from './ServiceList';

export default function Home() {
    return (
        <Container maxWidth="lg">
            <ServiceList />
        </Container>
    );
}