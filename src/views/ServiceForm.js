import React, {
	useEffect,
	createRef,
	useState,
} from 'react';
import {
	useHistory,
	useLocation,
	useParams,
} from "react-router-dom";
import {
    useSelector,
} from 'react-redux'

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Container, Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
// import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import ClearIcon from '@material-ui/icons/Clear';
import LinearProgress from '@material-ui/core/LinearProgress';

import UploadButton from '../components/UploadButton';
import { NumberFormatDefault as NumberFormat } from '../components/NumberFormatDefault';

import { CrudApi } from './../api';
import { useAuth } from "./../api/auth";
import scheme from './../api/scheme/service';
import _ from 'lodash';
import Loading from '../components/Loading';

const useStyles = makeStyles((theme) => ({
	card: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		[theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
			marginTop: theme.spacing(6),
			marginBottom: theme.spacing(6),
		},
	},
	paper: {
		// marginTop: theme.spacing(3),
		// marginBottom: theme.spacing(3),
		padding: theme.spacing(2),
		[theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
			// marginTop: theme.spacing(6),
			// marginBottom: theme.spacing(6),
			padding: theme.spacing(3),
		},
	},
	cover: {
		width: 150,
		height: 150,
		backgroundRepeat: 'no-repeat',
		backgroundColor:
			theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
		backgroundSize: 'cover',
		backgroundPosition: 'center',
		padding: theme.spacing(2),
		color: theme.palette.text.secondary,
	},
	form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
	},	
	linearProgress: {
		width: '100%',
		// '& > * + *': {
		  	// marginTop: theme.spacing(3),
		// },
	},
	buttonRem: {
        backgroundColor: red[500],
    },
}));

const coverUrl = (id) => {
	return id ? 
		`/api/service/${id}/cover/${Date.now()}` 
		: undefined;
}

export default function ServiceForm(props) {
    const theme = useTheme();
	const classes = useStyles(theme);
	
	const history = useHistory();
	const location = useLocation();
	const from = location.state && location.pathname === location.state.from ? 
		location.state.from 
		: { pathname: "/" };

	const { setAuthorization } = useAuth();
	const contentType = 'multipart/form-data';
	const api = CrudApi({
		baseUrl: '/api/service',
		scheme: scheme.normalization.normal,
		schemeNonSync: scheme.normalization.unsync,
		setAuthorization,
	});
	
	const { id } = useParams() || location.state || { id: props.entityId };
	const service = useSelector(state => 
		state.entities[scheme.normalization.normal.key]?.[id]
			|| scheme.default.default()
	);
	const isLoading = service.meta.isLoading;
	const [error, setError] = useState();
	const fileRef = createRef();
	const [cover, setCover] = useState(coverUrl(id));

	useEffect(() => {
			if (id) 
					api.read(id, () => setCover(coverUrl(id)));
			
		},
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[id]
	);

	const handleChange = (event) => {
		let updated = {
			...service,
			[event.target.name]: event.target.value,
		};

		if (!id) {
			updated = {
				...updated,
				meta: {
					isSync: false,
					isLoading: false,
					isError: false,
					error: null,
				}
			};
		}

		// CRUD_START > CRUD_READ > CRUD_PATCH (without call to server)
		api.edit(id, updated);
	};

	const handleAddPhoto = (event) => {
		event.preventDefault();

		const coverFile = fileRef?.current?.files[0];
		if (fileRef?.current?.files[0]) {
			const data = new FormData();
			data.append('cover', coverFile);
			id && api.patch(id, data, () => {
				setCover(coverUrl(id));
			}, {
				url: `/api/service/${id}/cover`,
				contentType,
			});
		}
	};

	const handleRemovePhoto = (event) => {
		event.preventDefault();

		const data = new FormData();
		id && api.patch(id, data, () => {
			setCover(coverUrl(id));
		}, {
			url: `/api/service/${id}/cover`,
			contentType,
		});
	};

	const handleSubmit = async event => {
		event.preventDefault();

		const values = _.omit(service, 'meta');

		const validation = id ? scheme.validation.existing : scheme.validation.create;
		validation.validate(values, {
			strict: true,
			abortEarly: false,
		}).then(values => {
				const data = new FormData();
				for (let [key, value] of Object.entries(values)) {
					if (value) {
						data.append(key, value);
					}
				}
				if (!id) {
					let cover = fileRef.current.files[0];
					if (cover) {
						data.append('cover', cover);
					}
				}

				onSubmit(id, data, contentType, onSuccess);
		})
		.catch(error => {
			let extracted = _.chain(error.inner)
				.keyBy(error => error.path)
				.mapValues(error => error.message)
				.value();
			setError(extracted);
		});
	}

	const onSuccess = () => {
		history.replace(from);
	}

	const onSubmit = async (id, data, contentType, onSuccessCallback) => {
		id ? 
			api.patch(id, data, onSuccessCallback, { contentType }) 
			: api.create(id, data, contentType, onSuccessCallback);
	}

	const coverGridSize = id ? 3 : 0;
	const coverStyle = cover ? {
		backgroundImage: `url(${cover})`,
	} : {};
	
	return (
		<Container maxWidth="md">
			{isLoading && (<Loading open={isLoading}/>)}
			<div className={classes.card}>
				{isLoading && (<LinearProgress className={classes.linearProgress}/>)}
				<Paper className={classes.paper}>
					<React.Fragment>
						<Typography variant="h6" gutterBottom>
							{service?.summary}
						</Typography>
						<form onSubmit={handleSubmit} encType={contentType} className={classes.form} noValidate>
							<Grid container spacing={3}>
								{id && (
									<Grid item xs={12} sm={coverGridSize}>
										<Paper className={classes.cover} style={coverStyle}/>
									</Grid>
								)}
								<Grid item xs={12} sm={12-coverGridSize}>
									<FormControl fullWidth error={error? true : false}>
										<Grid container spacing={3}>
											<Grid item xs={12} sm={9}>
												<TextField
													required
													id="summary"
													name="summary"
													label="Summary"
													fullWidth
													autoComplete="summary"
													value={service?.summary}
													onChange={handleChange}
													error={error?.summary ? true : false}
													helperText={error?.summary}
												/>
											</Grid>
											<Grid item xs={12} sm={3}>
												<TextField
													required
													id="amount"
													name="amount"
													label="Amount"
													fullWidth
													value={service?.amount}
													onChange={handleChange}
													InputProps={{
														inputComponent: NumberFormat,
													}}
													error={error?.amount ? true : false}
													helperText={error?.amount}
												/>
											</Grid>
											<Grid item xs={12} sm={12}>
												<TextField
													id="description"
													name="description"
													label="Description"
													fullWidth
													autoComplete="description"
													multiline
													rows={2}
													value={service?.description}
													onChange={handleChange}
													error={error?.description ? true : false}
													helperText={error?.description}
												/>
											</Grid>
										</Grid>
										{/* <FormHelperText>{error?.message}</FormHelperText> */}
									</FormControl>
								</Grid>
								<Grid item xs={12}>
									<Box display="flex" justifyContent="flex-start">
										<Box>
											<UploadButton
												id="cover"
												name="cover"
												fileRef={fileRef}
												accept="image/*"
												aria-label="Upload Photo"
												Icon={PhotoCamera}
												onChange={handleAddPhoto}
											/>
										</Box>
										<Box flexGrow={1}>
											<IconButton 
												aria-label="Remove Photo"
												className={classes.buttonRem} 
												component="span"
												onClick={handleRemovePhoto}
											>
												<ClearIcon />
											</IconButton>
										</Box>
										<Box alignSelf="flex-end">
											<Button
												type="submit"
												variant="contained"
												color="primary"
											>
												Confirm
											</Button>
										</Box>
									</Box>
								</Grid>
							</Grid>
						</form>
					</React.Fragment>
				</Paper>
			</div>
		</Container>
	);
}