import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
}));

export default function NoMatch(props) {
    const theme = useTheme();
    /*eslint-disable no-unused-vars*/
	const _classes = useStyles(theme);

    return (
        <Container maxWidth="sm">
            <Grid container justify="center">
                <Grid item xs={12} sm={6}>
                    <Typography variant="h1" component="h2" gutterBottom>
                        404
                    </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Typography variant="h2" component="h2" gutterBottom>
                        Oops! Page not found
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
}