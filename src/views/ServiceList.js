import React, {
    useEffect,
} from 'react';
import {
    Link as RouterLink,
    useLocation,
} from "react-router-dom";
import {
    useSelector,
} from 'react-redux'

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import Loading from './../components/Loading'
import ServiceCard from './../containers/service/ServiceCard';
import AddFabButton from './../components/AddFabButton';

import { CrudApi } from './../api';
import { useAuth } from "./../api/auth";
import scheme from './../api/scheme/service';

const useStyles = makeStyles((theme) => ({
}));

export default function ServiceList() {
    const theme = useTheme();
    /*eslint-disable no-unused-vars*/
    const classes = useStyles(theme);

    const location = useLocation();

	const { setAuthorization } = useAuth();

    const api = CrudApi({
        baseUrl: '/api/service',
        scheme: scheme.normalization.normal,
        schemeNonSync: scheme.normalization.unsync,
        setAuthorization,
    });

    const services = useSelector(state => state.entities[scheme.normalization.normal.key]);
    const isLoading = useSelector(state => state.isLoading);
    const error = useSelector(state => state?.error);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    useEffect(() => api.pull(), []);

    if (error) return <div>{error.details}</div>

    return (
        <React.Fragment>
            <Loading open={isLoading}/>
            <Grid container justify="flex-start" spacing={2}>
                {services && Object.values(services).map((service) => (
                    <Grid item key={service.id} xs={12} sm={6} md={6}>
                        <ServiceCard entityId={service.id}/>
                    </Grid>
                ))}
            </Grid>
            <RouterLink to={{
                pathname: "/api/service/add",
                state: { 
                    service: {
                        id: null,
                        summary: 'New Service',
                    },
                    from: location,
                },
			}}>
                <AddFabButton />
            </RouterLink>
        </React.Fragment>
    );
}