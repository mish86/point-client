import React, { 
	useState
} from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route,
} from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import defaultTheme from './theme/default';

import PrivateRoute from './routes/PrivateRoute';
import { AuthContext } from './api/auth';

import Home from './views/Home';
import LoginPage from './views/Login';
import SignUp from './views/SignUp';
import NoMatch from './views/NoMatch';
import ServiceForm from './views/ServiceForm';
import Loading from './components/Loading';

const useStyles = makeStyles((theme) => ({
}));

export default function App() {
	/*eslint-disable no-unused-vars*/
	const classes = useStyles(defaultTheme);

	const [authorization, setAuthorization] = useState({
		authorized: false,
		user: undefined,
	});

	return (
		<React.Fragment>
			<ThemeProvider theme={defaultTheme}>
				<CssBaseline />
				<main>
					<AuthContext.Provider value={{ authorization, setAuthorization }}>
						<Router>
							<Switch>
								<PrivateRoute exact path="/" children={<Home />} loading={Loading}/>
								<PrivateRoute path="/api/service/add" children={<ServiceForm />} loading={Loading}/>
								<PrivateRoute path="/api/service/:id" children={<ServiceForm />} loading={Loading}/>
								<Route path="/login" component={LoginPage} />
								<Route path="/signup" component={SignUp} />
								<Route path="*" component={NoMatch} />
							</Switch>
						</Router>
					</AuthContext.Provider>
				</main>
			</ThemeProvider>
		</React.Fragment>
	);
}