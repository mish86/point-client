import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
	},
}));

const Loading = props => {
	const theme = useTheme();
	const classes = useStyles(theme);

	const { open, showProgress } = props;
  
	return (
		<React.Fragment>
			<Backdrop className={classes.backdrop} open={open}>
				{showProgress && (<CircularProgress color="inherit" />)}
			</Backdrop>
		</React.Fragment>
	);
}

// export default React.forwardRef((props, ref) => <Loading {...props} ref={ref} />);
export default Loading;