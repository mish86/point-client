import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
		},
	},
	input: {
	  	display: 'none',
    },
    button: {
        backgroundColor: green[500],
    }
}));

export default function UploadButton({ Icon, ...props}) {
	const theme = useTheme();
	const classes = useStyles(theme);
  
	return (
	  	<div className={classes.root}>
			<input 
				accept={props.accept}
				className={classes.input}
				id={props.id}
				name={props.name}
				ref={props.fileRef}
				type="file"
				onChange={props.onChange}
			/>
			<label htmlFor={props.id}>
				<IconButton className={classes.button} aria-label={props["aria-label"]} component="span">
					<Icon />
				</IconButton>
			</label>
	  	</div>
	);
}