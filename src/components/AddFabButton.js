import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { green } from '@material-ui/core/colors';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
    fab: {
        position: "fixed",
        zIndex: 1,
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
    fabGreen: {
        color: theme.palette.common.white,
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[600],
        },
    },
}));

export default function AddFabButton() {
    const theme = useTheme();
    const classes = useStyles(theme);

    const fab = {
        color: 'inherit',
        className: clsx(classes.fab, classes.fabGreen),
        icon: <AddIcon />,
        label: 'Add',
    };

    return (
        <Fab 
            aria-label={fab.label} 
            className={fab.className} 
            color={fab.color}
        >
            {fab.icon}
        </Fab>
    );
}