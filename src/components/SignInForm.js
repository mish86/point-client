import React from 'react';
import {
	Link as RouterLink,
} from 'react-router-dom';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Loading from './Loading';

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignInFormComponent(props) {
	const theme = useTheme();
	const classes = useStyles(theme);
	
    const { dto, isLoading, isOverallError, error, handleFocus, handleChange, handleSubmit } = props;
    
    return (
		<>
			<Loading open={isLoading} />

			<form onSubmit={handleSubmit} className={classes.form} noValidate>
				<FormControl fullWidth error={isOverallError}>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						id="username"
						label="Email Address"
						name="username"
						autoComplete="email"
						autoFocus
						value={dto['username']}
						onChange={handleChange}
						onFocus={handleFocus}
						error={error['username']?.isError}
						helperText={error['username']?.message}
					/>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						name="password"
						label="Password"
						type="password"
						id="password"
						autoComplete="current-password"
						value={dto['password']}
						onChange={handleChange}
						onFocus={handleFocus}
						error={error['password']?.isError}
						helperText={error['password']?.message}
					/>
					{isOverallError && (<FormHelperText>{error.detail}</FormHelperText>)}
				</FormControl>
				<Button
					type="submit"
					fullWidth
					variant="contained"
					color="primary"
					className={classes.submit}
				>
					Sign In
				</Button>
				<Grid container>
					<Grid item xs>
						<Link href="#" variant="body2">
							Forgot password?
						</Link>
					</Grid>
					<Grid item>
						{/* TODO take RouterLink from props */}
						<Link component={RouterLink} to="/signup" variant="body2">
							Sign Up
						</Link>
					</Grid>
				</Grid>
			</form>
		</>
    );
}
