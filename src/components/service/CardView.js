import React from 'react';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import Loading from '../Loading';

const useStyles = makeStyles((theme) => ({
	card: {
		display: 'flex',
		height: 150,
	},
	photo: {
		width: 150,
		// height: 150,
	},
	details: {
		flex: 1,
		display: 'flex',
		flexDirection: 'column',
	},
	content: {
		// flex: '1 0 auto',
		width: '100%',
		height: '75%',
		'overflow-y': 'auto',
	},	
	controls: {
		display: 'flex',
		alignItems: 'center',
		paddingLeft: theme.spacing(1),
		// paddingBottom: theme.spacing(1),
	},
	linearProgress: {
		width: '100%',
		'& > * + *': {
		  marginTop: theme.spacing(2),
		},
	},
}));

export default function CardView(props) {
    const theme = useTheme();
    const classes = useStyles(theme);
	
	const { service, photoHref, handleEdit, handleDelete, editTo, deleteTo, RouterLink } = props;
    const isLoading = service.meta.isLoading;
    
    return (
		<>
			{isLoading && (<Loading open={isLoading} />)}
			
			{isLoading && (<LinearProgress className={classes.linearProgress}/>)}

			<Card className={classes.card}>
				<CardMedia
					className={classes.photo}
					image={photoHref}
				/>
				<div className={classes.details}>
					<CardContent className={classes.content}>
						<Typography variant="subtitle1">
							{service.summary}
						</Typography>
						<Typography variant="caption">
							{service.description}
						</Typography>
					</CardContent>	
					<div className={classes.controls}>
						<Button size="small">
							Book
						</Button>
						<RouterLink to={editTo}>
							<IconButton 
								aria-label="Edit" 
								color="primary"
								onClick={handleEdit}
							>
								<EditIcon />
							</IconButton>
						</RouterLink>
						<RouterLink to={deleteTo}>
							<IconButton 
								aria-label="Delete"
								color="secondary"
								onClick={handleDelete}
							>
								<DeleteIcon />
							</IconButton>
						</RouterLink>
					</div>
				</div>
			</Card>
		</>
    );
}
