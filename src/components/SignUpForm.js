import React from 'react';
import {
    Link as RouterLink,
} from "react-router-dom";

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';

import { makeStyles, useTheme } from '@material-ui/core/styles';

import Loading from './../components/Loading';

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignUpComponent(props) {
    const theme = useTheme();
    const classes = useStyles(theme);

    const { dto, isLoading, isOverallError, error, handleFocus, handleChange, handleSubmit } = props;
    
    return (
        <>
            <Loading open={isLoading} />

            <form onSubmit={handleSubmit} className={classes.form} noValidate>
                <FormControl fullWidth error={isOverallError}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="realname"
                                label="First Name"
                                name="realname"
                                autoComplete="fname"
                                value={dto['realname']}
                                onChange={handleChange}
                                onFocus={handleFocus}
                                error={error['realname']?.isError}
                                helperText={error['realname']?.message}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="username"
                                label="Email Address"
                                name="username"
                                autoComplete="email"
                                value={dto['username']}
                                onChange={handleChange}
                                onFocus={handleFocus}
                                error={error['username']?.isError}
                                helperText={error['username']?.message}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={dto['password']}
                                onChange={handleChange}
                                onFocus={handleFocus}
                                error={error['password']?.isError}
                                helperText={error['password']?.message}
                            />
                        </Grid>
                    </Grid>
                    {isOverallError && (<FormHelperText>{error.detail}</FormHelperText>)}
                </FormControl>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Sign Up
                </Button>
                <Grid container justify="flex-end">
                    <Grid item>
                        {/* TODO take RouterLink from props */}
                        <Link component={RouterLink} to="/login" variant="body2">
                            Already have an account? Sign in
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </>
    );
}