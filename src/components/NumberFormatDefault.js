import React, {
} from 'react';

import NumberFormat from 'react-number-format';

export function NumberFormatDefault(props) {
	const { inputRef, onChange, ...other } = props;
  
	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={({ value, floatValue, formattedValue }) => {
				onChange({
					target: {
						name: props.name,
						value: floatValue,
					},
				});
			}}
			thousandSeparator=' '
			allowNegative={false}
			isNumericString
			prefix='₽ '
		/>
	);
}