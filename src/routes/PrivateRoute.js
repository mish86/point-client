import React, { useEffect } from 'react';
import {
	Route,
	Redirect,
} from 'react-router-dom';
import { useAuth, Ping } from "./../api/auth";

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export default function PrivateRoute({ children, loading, ...rest }) {
	const { authorization: { authorized } } = useAuth();
	const { isLoading, ping } = Ping();

	const Loading = loading;
	
	// eslint-disable-next-line react-hooks/exhaustive-deps
	useEffect(() => ping(), []);

	if (isLoading) return <div>{<Loading open={isLoading}/>}</div>

	return (
		<Route
			{...rest}
			render={({ location }) => 
				authorized ? (
					children
				) : (
					<Redirect
						to={{
							pathname: "/login",
							state: { from: location }
						}}
					/>
				)
			}
		/>
	);
}