// import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';

const palette = {
    type: 'dark',
    primary: blue,
    secondary: pink,
};

const themeName = 'San Marino Razzmatazz Babirusa';

const theme = createMuiTheme({ palette, themeName });

export default theme;